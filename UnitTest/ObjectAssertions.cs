﻿using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unit.Tests;

namespace UnitTest
{
    public class ObjectAssertions<T> where T : class
    {
        private readonly object _actualObject;
        private PropertyInfo[] _propertyInfoToCompare;

        public ObjectAssertions(object actualObject)
        {
            _actualObject = actualObject;
        }

        public ObjectAssertions<T> Properties()
        {
            _propertyInfoToCompare = _actualObject.GetType().GetProperties();

            return this;
        }

        public ObjectAssertions<T> PropertiesWithout(Expression<Func<T, string>> outExpr)
        {
            var expr = (MemberExpression)outExpr.Body;
            var prop = (PropertyInfo)expr.Member;
            _propertyInfoToCompare = _actualObject.GetType().GetProperties().Where(p => p != prop).ToArray();

            return this;
        }

        public void Eq(object expected)
        {
            IList diffProperties = new ArrayList();

            foreach (var item in _propertyInfoToCompare)
            {
                if (!Equals(item.GetValue(_actualObject, null), expected.GetType().GetProperty(item.Name).GetValue(expected, null)))
                {
                    diffProperties.Add(item);
                }
            }

            if (diffProperties.Count > 0)
            {
                throw new ExpectationFailedException();
            }

            Assert.IsTrue(true);
        }
    }
}
