﻿using System;
using Unit.Tests;

namespace UnitTest
{
    public static class AssertionExtensions
    {
        public static NumericAssertions<int> Expect(this int actualValue)
        {
            return new NumericAssertions<int>(actualValue);
        }

        public static ActionAssertions Expect(this Action action)
        {
            return new ActionAssertions(action);
        }

        public static ObjectAssertions<FizBar> Expect(this object actualObject) 
        {
            return new ObjectAssertions<FizBar>(actualObject);
        }
    }
}
