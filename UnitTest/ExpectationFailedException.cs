﻿using System;

namespace UnitTest
{
    public class ExpectationFailedException : Exception
    {
        public ExpectationFailedException()
        {
        }

        public ExpectationFailedException(string message)
        : base(message)
        {
        }

        public ExpectationFailedException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
