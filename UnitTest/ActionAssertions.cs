﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    public class ActionAssertions
    {
        private readonly Action _action;

        public ActionAssertions(Action action)
        {
            _action = action;
        }

        public void RaiseError()
        {
            bool hasExceptionBeenThrown = false;
            try
            {
                _action();
            }
            catch (Exception)
            {
                hasExceptionBeenThrown = true;
                Assert.IsTrue(true);
            }
            finally
            {
                if (!hasExceptionBeenThrown)
                {
                    throw new ExpectationFailedException();
                }
            }
        }
    }
}
