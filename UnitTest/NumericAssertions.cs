﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    public class NumericAssertions<T> where T : struct
    {
        private readonly IComparable _actual;
        private bool _negate;

        public NumericAssertions(object value)
        {
            if (!ReferenceEquals(value, null))
            {
                _actual = value as IComparable;
                if (_actual == null)
                {
                    throw new InvalidOperationException("This class only supports types which implements IComparable.");
                }
            }
        }

        public NumericAssertions<T> Not()
        {
            _negate = !_negate;

            return this;
        }

        public void Eq(T expected)
        {
            try
            {
                if (_negate)
                {
                    Assert.AreNotEqual(_actual, expected);
                }
                else
                {
                    Assert.AreEqual(_actual, expected);
                }
                
            }
            catch (Exception e)
            {
                throw new ExpectationFailedException("Assertion failed", e);
            }
        }

        public void IsGreater(T expected)
        {
            try
            {
                if (_negate)
                {
                    Assert.IsFalse(_actual.CompareTo(expected) > 0);
                }
                else
                {
                    Assert.IsTrue(_actual.CompareTo(expected) > 0);
                }
            }
            catch (Exception e)
            {
                throw new ExpectationFailedException("Assertion failed", e);
            }
        }
    }
}
